import 'package:api/Logic/API/Models/contactModel.dart';
import 'package:flutter/material.dart';

class ContactView extends StatelessWidget {

  final ContactModel model;


  ContactView(this.model);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Icon(Icons.person),
        title: Text(model.name),
        subtitle: Text(model.phone),
      ),
    );
  }
}
