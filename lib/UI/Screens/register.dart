// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, curly_braces_in_flow_control_structures

import 'package:api/Logic/API/Controllers/AuthController.dart';
import 'package:flutter/material.dart';

class RegisterScreen extends StatelessWidget {

  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController passController = TextEditingController();



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('register'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            TextField(
              decoration: InputDecoration(
                hintText: 'name'
              ),
              controller: nameController,
            ),
            TextField(
              decoration: InputDecoration(
                  hintText: 'email'
              ),
              controller: emailController,
            ),
            TextField(
              decoration: InputDecoration(
                  hintText: 'phone'
              ),
              controller: phoneController,
            ),
            TextField(
              decoration: InputDecoration(
                  hintText: 'password',
              ),
              controller: passController,
              obscureText: true,
            ),
            Builder(
              builder: (context) {
                return ElevatedButton(
                    onPressed: () async {
                      var result = await AuthController.register(
                          name: nameController.text,
                          email: emailController.text,
                          phone: phoneController.text,
                          password: passController.text
                      );

                      if(result)
                      Navigator.pushReplacementNamed(context, '/home');
                      else
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Failed')));

                    },
                    child: Text('register')
                );
              }
            )
          ],
        ),
      ),
    );
  }
}
