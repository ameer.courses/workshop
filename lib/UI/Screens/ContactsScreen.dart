// ignore_for_file: curly_braces_in_flow_control_structures, prefer_const_constructors

import 'package:api/Logic/API/Controllers/ContactsController.dart';
import 'package:api/Logic/API/Models/contactModel.dart';
import 'package:api/UI/Widgets/ContactView.dart';
import 'package:flutter/material.dart';


class ContactsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contacts'),
      ),
      body: FutureBuilder<List<ContactModel>>(
        future: ContactsController.getAllContacts(),
        builder: (context,snapShot){
          if(snapShot.hasData)
            return ListView.builder(
              itemCount: snapShot.data!.length,
              itemBuilder: (context,i){
                return ContactView(snapShot.data![i]);
              },
            );

          if(snapShot.hasError)
            return Center(
              child: Text(snapShot.error.toString()),
            );

          return Center(
            child: CircularProgressIndicator(),
          );

        },
      ),
    );
  }
}
