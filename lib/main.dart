import 'package:api/UI/Screens/register.dart';
import 'package:flutter/material.dart';

import 'UI/Screens/ContactsScreen.dart';
import 'UI/Screens/home.dart';


void main() {


  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/home' : (c) => HomeScreen(),
        '/register' : (c) => RegisterScreen()
      },
      home: RegisterScreen()
    );
  }
}
