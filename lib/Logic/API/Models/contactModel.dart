

class ContactModel{
  final String name;
  final String phone;

  ContactModel({
    required this.name,
    required this.phone
  });

  factory ContactModel.fromJSON(Map<String,dynamic> json) =>
     ContactModel(
      name: json['name'],
      phone: json['phone']
    );


}