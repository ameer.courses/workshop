// ignore_for_file: curly_braces_in_flow_control_structures

import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:api/Logic/API/Models/contactModel.dart';

class ContactsController{

  static Future<List<ContactModel>> getAllContacts() async{
    var response = await http.get(Uri.parse('https://jsonkeeper.com/b/UMEN/'));
    Map<String,dynamic> json = jsonDecode(response.body);
    List contacts = json['contacts'];
    List<ContactModel> models = [];
    for(var j in contacts)
      models.add(
          ContactModel.fromJSON(j)
      );

    return models;
  }




}