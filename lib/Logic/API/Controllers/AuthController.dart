// ignore_for_file: unused_local_variable

import 'package:api/Logic/API/Controllers/Controller.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class AuthController{

  static Future<bool> register({required String name, required String email, required String phone, required String password,}) async {
    var response = await http.post(
        Uri.parse('${Controller.ip}register'),
      body:jsonEncode({
        'name':name,
        'email':email,
        'phone' : phone,
        'password' : password
      }),
      headers: {
        'Content-Type': 'application/json'
      }
    );
    Map<String,dynamic> json = jsonDecode(response.body);

    print(json);

    return json['token'] != null;


  }




}